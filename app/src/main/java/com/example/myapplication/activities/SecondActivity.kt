package com.example.myapplication.activities

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.myapplication.R
import com.example.myapplication.adapters.RecyclerAdapter

class SecondActivity : AppCompatActivity() {

    private var layoutManager : RecyclerView.LayoutManager? = null
    private var adapter : RecyclerView.Adapter<RecyclerAdapter.ViewHolder>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_second)

        layoutManager = LinearLayoutManager(this)
        findViewById<RecyclerView>(R.id.recyclerViewActivity).layoutManager = layoutManager

        adapter = RecyclerAdapter()
        findViewById<RecyclerView>(R.id.recyclerViewActivity).adapter = adapter

    }
}