package com.example.myapplication.activities

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.myapplication.R


class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

    }

    fun sendMessage(view: View) {
        val viewText = findViewById<TextView>(R.id.textView6)
        val editText = findViewById<TextView>(R.id.editTextTextPersonName)
        viewText.setText(editText.text)

        val toast = Toast.makeText(applicationContext, getString(R.string.app_name), Toast.LENGTH_SHORT)
        toast.show()
    }

    fun switchScreen(view: View)
    {
        val myIntent = Intent(this, SecondActivity::class.java)
        //val myIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        startActivity(myIntent)
    }
}